    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="utf-8">
    <title>Alireza Beikverdi :: Skills</title>
    <link rel="shortcut icon" href="/img/logo.ico" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="alireza, beikverdi, personal website, web developer, mobile developer, portfolio, researcher">
    <meta name="author" content="Alireza Beikverdi">
    <meta name="description" content="This is a personal website of Alireza Beikverdi">
    <!-- styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="font/css/fontello.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
    <div class="navbar">
      <div class="navbar-inner">
        <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand" href="index.php"><img src="img/Ali.jpg"/></a>
          <ul class="nav nav-collapse pull-right">
            <li><a href="index.php"><i class="icon-user"></i> Profile</a></li>
            <li><a href="publications.php" class="active"><i class="icon-trophy"></i> Education</a></li>
            <li><a href="work.php"><i class="icon-picture"></i> Work</a></li>
          </ul>
          <!-- Everything you want hidden at 940px or less, place within here -->
          <div class="nav-collapse collapse">
            <!-- .nav, .navbar-search, .navbar-form, etc -->
          </div>
        </div>
      </div>
    </div>
    <!--Skills container-->
      
      <div class="container profile">
        <p>
          <h2>Education</h2>
          <ul>
            <li>
              Master of Science (M.Sc.), Computer Science, Yonsei University, South Korea (2014-2016)  
            </li>
            <li>
              B.S. in Information Technology, Software Engineering, Multimedia University, Malaysia
            </li>
          </ul>
            <br>
            <br>
          <h2>Publications</h2>
          2014
          <hr/>
          <ul>
            <li>
             Alireza Beikverdi, InHwan Kim, JooSeok Song, Centralized Payment System Using Social Networks Account, IEEE International Conference on Social Computing and Networking (Socialcom 2014), Sydney, Australia, December 2014.
            </li>
          </ul>
          <br>
          2012
          <hr/>
          <ul>
            <li>
             Alireza Beikverdi, Ian K. T. Tan, Improved Look-Ahead Re-Synchronization Window For HMAC-Based One-Time Password, IET International Conference on Wireless Communications and Applications (ICWCA 2012), Kuala Lumpur, Malaysia, October 2012. pp. 1 - 5
            </li>
            <li>
              Mostafa Abedi, Alireza Beikverdi, Rise of Massive Open Online Courses, International Congress on Engineering Education (ICEED), 2012 IEEE, Penang, Malaysia 
            </li>
          </ul>
         
        </p>
      </div>
    <!--END: Skills container-->
    <!-- Social Icons -->
    <div class="row social">
      <ul class="social-icons">
        <li><a href="#" target="_blank"><img src="img/fb.png" alt="facebook"></a></li>
        <li><a href="#" target="_blank"><img src="img/tw.png" alt="twitter"></a></li>
        <li><a href="#" target="_blank"><img src="img/go.png" alt="google plus"></a></li>
        <li><a href="#" target="_blank"><img src="img/pin.png" alt="pinterest"></a></li>
        <li><a href="#" target="_blank"><img src="img/st.png" alt="stumbleupon"></a></li>
        <li><a href="#" target="_blank"><img src="img/dr.png" alt="dribbble"></a></li>
      </ul>
    </div>
    <!-- END: Social Icons -->
    <!-- Footer -->
    <div class="footer">
      <div class="container">
        <p class="pull-left" style="color:#aaa;">© 2015 Alireza Beikverdi. All Rights Reserved.</p>
        <p class="pull-right"><a href="#myModal" role="button" data-toggle="modal"> <i class="icon-mail"></i> CONTACT</a></p>
      </div>
    </div>
    <!-- Contact form in Modal -->
    <!-- Modal -->
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><i class="icon-mail"></i> Contact Me</h3>
      </div>
      <div class="modal-body">
        <?php
        // display form if user has not clicked submit
        if (!isset($_POST["submit"])) {
        ?>
        <form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
          <input type="text" name="from" placeholder="Your Name">
          <input type="text" name="email" placeholder="Your Email">
          <input type="text" name="subject" placeholder="Subject">
          <textarea rows="3" name="message" style="width:80%"></textarea>
          <br/>
          <button type="submit" name="submit" class="btn btn-large"><i class="icon-paper-plane"></i> SUBMIT</button>
        </form>
        <?php 
        } else {    // the user has submitted the form
          // Check if the "from" input field is filled out
          if (isset($_POST["from"])) {
            $from = $_POST["from"]; // sender
            $email = $_POST["email"];
            $subject = $_POST["subject"];
            $message = $_POST["message"];
            // message lines should not exceed 70 characters (PHP rule), so wrap it
            $message = wordwrap($message, 100);
            $message = "From : " . $from . "\n" . $message;
            // send mail
            $header = 'From: ' . $email . "\r\n" .
            'Reply-To: alireza.be' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            mail("email@alireza.be",$subject,$message,$header);
            echo "Thank you for sending us feedback";
          }
        }
        ?>
      </div>
    </div>
    <!-- Scripts -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
            $('#myModal').modal('hidden')
    </script>
    </body>
    </html>

    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="utf-8">
    <title>Alireza Beikverdi :: Profile</title>
    <link rel="shortcut icon" href="/img/logo.ico" >
    <meta name="keywords" content="alireza, beikverdi, personal website, web developer, mobile developer, portfolio, researcher">
    <meta name="author" content="Alireza Beikverdi">
    <meta name="description" content="This is a personal website of Alireza Beikverdi">
    <!-- styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="font/css/fontello.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
    <div class="navbar">
      <div class="navbar-inner">
        <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand" href="index.php"><img src="img/Ali.jpg"/></a>
          <ul class="nav nav-collapse pull-right">
            <li><a href="index.php" class="active"><i class="icon-user"></i> Profile</a></li>
            <li><a href="publications.php"><i class="icon-trophy"></i> Education</a></li>
            <li><a href="work.php"><i class="icon-picture"></i> Work</a></li>
          </ul>
          <!-- Everything you want hidden at 940px or less, place within here -->
          <div class="nav-collapse collapse">
            <!-- .nav, .navbar-search, .navbar-form, etc -->
          </div>
        </div>
      </div>
    </div>
    <!--Profile container-->
    <div class="container profile">
      <div class="span3"> <img src="img/Ali.jpg" alt="Profile Picture" style="width: 250px; height: 290px;"> </div>
      <div class="span5">
        <h1>Alireza Beikverdi</h1>
        <h3>Software Engineer/Application Developer</h3>
        <p>A Creative software and computer expert, startup enthusiast, part time web and mobile app developer and on top, a cryptocurrency believer.<br>
          Currently researching on Bitcoin and Bitcoin 2.0 technologies as for my master thesis at Yonsei University in Seoul, South Korea.<br>
          Open to technology and freelancing any projects and services so feel free to contact me.

        </p>
        <a href="#myModal" class="hire-me" data-toggle="modal"><i class="icon-paper-plane"> </i> Contact Me </a> </div>
    </div>
    <!--END: Profile container-->
    <!-- Social Icons -->
    <div class="row social">
      <ul class="social-icons">
        <li><a href="#" target="_blank"><img src="img/fb.png" alt="facebook"></a></li>
        <li><a href="https://twitter.com/abeikverdi" target="_blank"><img src="img/tw.png" alt="twitter"></a></li>
        <li><a href="https://plus.google.com/+AlirezaBeikverdi" target="_blank"><img src="img/go.png" alt="google plus"></a></li>
        <li><a href="#" target="_blank"><img src="img/pin.png" alt="pinterest"></a></li>
        <li><a href="#" target="_blank"><img src="img/st.png" alt="stumbleupon"></a></li>
        <li><a href="https://abeikverdi.socialdoe.com/" target="_blank"><img src="img/dr.png" alt="dribbble"></a></li>
      </ul>
    </div>
    <!-- END: Social Icons -->
    <!-- Footer -->
    <div class="footer">
      <div class="container">
        <p class="pull-left" style="color:#aaa;">© 2015 Alireza Beikverdi. All Rights Reserved.</p>
        <p class="pull-right"><a href="#myModal" role="button" data-toggle="modal"> <i class="icon-mail"></i> CONTACT</a></p>
      </div>
    </div>
    <!-- Contact form in Modal -->
    <!-- Modal -->
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><i class="icon-mail"></i> Contact Me</h3>
      </div>
      <div class="modal-body">
        <?php
        // display form if user has not clicked submit
        if (!isset($_POST["submit"])) {
        ?>
        <form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
          <input type="text" name="from" placeholder="Your Name">
          <input type="text" name="email" placeholder="Your Email">
          <input type="text" name="subject" placeholder="Subject">
          <textarea rows="3" name="message" style="width:80%"></textarea>
          <br/>
          <button type="submit" name="submit" class="btn btn-large"><i class="icon-paper-plane"></i> SUBMIT</button>
        </form>
        <?php 
        } else {    // the user has submitted the form
          // Check if the "from" input field is filled out
          if (isset($_POST["from"])) {
            $from = $_POST["from"]; // sender
            $email = $_POST["email"];
            $subject = $_POST["subject"];
            $message = $_POST["message"];
            // message lines should not exceed 70 characters (PHP rule), so wrap it
            $message = wordwrap($message, 100);
            $message = "From : " . $from . "\n" . $message;
            // send mail
            $header = 'From: ' . $email . "\r\n" .
            'Reply-To: alireza.be' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            mail("email@alireza.be",$subject,$message,$header);
            echo "Thank you for sending us feedback";
          }
        }
        ?>
      </div>
    </div>
    <!-- Scripts -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
                $('#myModal').modal('hidden')
        </script>
        
    </body>
    </html>

  

    <!--
<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
<script type="IN/MemberProfile" data-id="http://www.linkedin.com/in/abeikverdi" data-format="inline" data-related="false"></script>
-->
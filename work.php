    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="utf-8">
    <title>Alireza Beikverdi :: Work</title>
    <link rel="shortcut icon" href="/img/logo.ico" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="alireza, beikverdi, personal website, web developer, mobile developer, portfolio, researcher">
    <meta name="author" content="Alireza Beikverdi">
    <meta name="description" content="This is a personal website of Alireza Beikverdi">  
  <!-- styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="font/css/fontello.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <!-- scripts -->
    <!-- Add jQuery library -->
    <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="js/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css?v=2.1.5" media="screen" />
    <script>
            $(document).ready(function() {
        $(".fancybox-thumb").fancybox({
            helpers	: {
                title	: {
                    type: 'inside'
                },
                overlay : {
                            css : {
                                'background' : 'rgba(1,1,1,0.65)'
                            }
                        }
            }
        });
    });
        </script>
    </head>
    <body>
    <div class="navbar">
      <div class="navbar-inner">
        <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand" href="index.php"><img src="img/Ali.jpg"/></a>
          <ul class="nav nav-collapse pull-right">
            <li><a href="index.php"><i class="icon-user"></i> Profile</a></li>
            <li><a href="publications.php"><i class="icon-trophy"></i> Education</a></li>
            <li><a href="work.php" class="active"><i class="icon-picture"></i> Work</a></li>
          </ul>
          <!-- Everything you want hidden at 940px or less, place within here -->
          <div class="nav-collapse collapse">
            <!-- .nav, .navbar-search, .navbar-form, etc -->
          </div>
        </div>
      </div>
    </div>
    <!-- Works container -->
    <div class="container work">
      <h2>Recent Projects</h2>
      <ul class="work-images">
        <li>
          <div><a class="fancybox-thumb" rel="fancybox-thumb" href="img/4.jpg" title="SKIP is a centralized payment system using social networks. It can be integrated with any social networks to send and receive money from the user. You can refer to my publications and read more about this system."> <img src="img/4-thumb.jpg"/></a></div>
                  <br>
          <p>SKIP social networking payment system</p>
        </li>

        <li>
          <div><a class="fancybox-thumb" rel="fancybox-thumb" href="img/2.jpg" title="Hilti is one of the biggest construction company in the world with over 20 apps on Android and iOS for their clients and employees. I developed an Android and iOS library that was added to Hilti mobile app store that track users usage and publish them to the Hilti Business Intelligence through RESTful service."><img src="img/2-thumb.jpg" /></a></div>
          <br>
          <p>Hilti app tracker</p>
        </li>

        <li>
          <div><a class="fancybox-thumb" rel="fancybox-thumb" href="img/3.jpg" title="Jatomi is a Polish fitness that operating in 7 countries in the world. We developed a website and a service which handles users profile and integrated it with Jatomis local user database. It trackes their activities and recommend users many promotions and motivate them by giving them badges and ... Also users are able to book personal trainers, check their schedule and many other features."><img src="img/3-thumb.jpg" /></a></div>
          <br>
          <p>Jatomi fitness website</p>
        </li>

      </ul>
      <ul class="work-images">
        <li>
          <div><a class="fancybox-thumb" rel="fancybox-thumb" href="img/1.jpg" title="Windows phone HMAC-Based one-time password token with with a .Net server validator. 
            I published a paper that introduces an efficient way with better performance system."><img src="img/1-thumb.jpg" /></a></div>
          <br>
          <p>OTP generator and validator</p>
        </li>
        <li>
          <div><a class="fancybox-thumb" rel="fancybox-thumb" href="img/5.jpg" title="CubeStudio is a startup company with a group of creative entropreneurs in Korea that introduce innovative app and website services."><img src="img/5-thumb.jpg" /></a></div>
          <br>
          <p>CubeStudio</p>
        </li>

        <li>
          <div><a class="fancybox-thumb" rel="fancybox-thumb" href="img/6.jpg" title="Timeit is an Android and iOS app for tracking and logging your time to make the most out of your time in a day. It gives you interactive statistics of users activities."><img src="img/6-thumb.jpg" /></a></div>
          <br>
          <p>Timeit</p>
        </li>
      </ul>
    
    </div>
    <!--END: Work container-->
    <!-- Social Icons -->
    <div class="row social">
      <ul class="social-icons">
        <li><a href="#" target="_blank"><img src="img/fb.png" alt="facebook"></a></li>
        <li><a href="#" target="_blank"><img src="img/tw.png" alt="twitter"></a></li>
        <li><a href="#" target="_blank"><img src="img/go.png" alt="google plus"></a></li>
        <li><a href="#" target="_blank"><img src="img/pin.png" alt="pinterest"></a></li>
        <li><a href="#" target="_blank"><img src="img/st.png" alt="stumbleupon"></a></li>
        <li><a href="#" target="_blank"><img src="img/dr.png" alt="dribbble"></a></li>
      </ul>
    </div>
    <!-- END: Social Icons -->
    <!-- Footer -->
    <div class="footer">
      <div class="container">
        <p class="pull-left" style="color:#aaa;">© 2015 Alireza Beikverdi. All Rights Reserved.</p>
        <p class="pull-right"><a href="#myModal" role="button" data-toggle="modal"> <i class="icon-mail"></i> CONTACT</a></p>
      </div>
    </div>
    <!-- Contact form in Modal -->
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><i class="icon-mail"></i> Contact Me</h3>
      </div>
      <div class="modal-body">
        <?php
        // display form if user has not clicked submit
        if (!isset($_POST["submit"])) {
        ?>
        <form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
          <input type="text" name="from" placeholder="Your Name">
          <input type="text" name="email" placeholder="Your Email">
          <input type="text" name="subject" placeholder="Subject">
          <textarea rows="3" name="message" style="width:80%"></textarea>
          <br/>
          <button type="submit" name="submit" class="btn btn-large"><i class="icon-paper-plane"></i> SUBMIT</button>
        </form>
        <?php 
        } else {    // the user has submitted the form
          // Check if the "from" input field is filled out
          if (isset($_POST["from"])) {
            $from = $_POST["from"]; // sender
            $email = $_POST["email"];
            $subject = $_POST["subject"];
            $message = $_POST["message"];
            // message lines should not exceed 70 characters (PHP rule), so wrap it
            $message = wordwrap($message, 100);
            $message = "From : " . $from . "\n" . $message;
            // send mail
            $header = 'From: ' . $email . "\r\n" .
            'Reply-To: alireza.be' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            mail("email@alireza.be",$subject,$message,$header);
            echo "Thank you for sending us feedback";
          }
        }
        ?>
      </div>
    </div>
    <!-- Scripts -->
    <script src="js/bootstrap.min.js"></script>
    <script>
            $('#myModal').modal('hidden')
    </script>
    </body>
    </html>
